package Human;
import org.junit.After;

import static org.junit.Assert.*;

public class HumanTest {
    private Human human;
    private Human sameHuman;
    private Human secondHuman;
    private Human thirdHuman;
    @org.junit.Before
    public void init() {
        human = new Human("name","surname");
        sameHuman = human;
        secondHuman = new Human("name","surname");
        thirdHuman = new Human("n","a");
    }
    @org.junit.After
    public void tearDown() {
        human = null;
    }
    @org.junit.Test
    public void testToString() {
        assertTrue(human.toString(),true);
    }

    @org.junit.Test
    public void testHashCode() {
        assertEquals("this 2 obj has the same hash code",human,sameHuman);
        assertNotEquals("this 2 obj has not the same hash code",human,secondHuman);
    }

    @org.junit.Test
    public void testEquals() {
        assertEquals("This 2 obj is equal",human,secondHuman);
        assertNotEquals("This 2 obj is not equal",human,thirdHuman);
    }
}
