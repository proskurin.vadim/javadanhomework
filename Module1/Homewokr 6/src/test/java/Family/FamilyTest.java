package Family;

import Human.Human;

import static org.junit.Assert.*;
public class FamilyTest {

    private Family family;
    private Human c1;
    private int count;
    @org.junit.Before
    public void init() {
        Human m = new Human("m","h");
        Human f = new Human("f","h");
        c1 = new Human("c1","h");
        Human c2 = new Human("c2","h");
        Human c3 = new Human("c3","h");
        family = new Family(m,f);
        family.addChildren(c1);
        family.addChildren(c2);
        family.addChildren(c3);
        count = family.countFamily();
    }
    @org.junit.After
    public void tearDown() {
        family = null;
    }

    @org.junit.Test
    public void testAddChildren() {
        Human c4 = new Human("c4","h");
        family.addChildren(c4);
        assertEquals("add child",count+1,family.countFamily());
    }

    @org.junit.Test
    public void testDeleteChildren() {
        family.deleteChildren(c1);
        assertEquals("dell child",count,count-1);
    }

    @org.junit.Test
    public void testCountFamily() {
        assertEquals("equals",count,5);
        assertNotEquals("not equals",count,55);
    }
}
