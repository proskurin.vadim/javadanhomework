package Pet;
import static org.junit.Assert.*;

public class PetTest {
    private Pet pet;
    @org.junit.Before
    public void init() {
        pet = new Pet(Pet.Species.DOG,"pet");
    }
    @org.junit.After
    public  void tearDown() {
        pet = null;
    }
    @org.junit.Test
    public void testToString() {
        assertTrue(pet.toString(),true);
    }
}
