
import Pet.Pet;
import Human.Human;
import Family.Family;
public class Main {
    public static void main(String[] args) {
        Human father = new Human("Леон", "Рене");
        Human mother = new Human("Натали", "Портман");
        Pet pet3 = new Pet(Pet.Species.DOG,"Леди",5,50,new String[]{"Любит старков","Спит после 8"});
        System.out.println(pet3.toString());
        Human children = new Human("имя", "фамилия",1980 , 70, pet3);
        Human children2 = new Human("Vlad","Renyu");
        Human nonChildren = new Human("Vadim","None");
        Family fam1 = new Family(mother,father);
        pet3.foul();
        pet3.respond();
        pet3.foul();
        System.out.println(pet3.toString());
        children.feedPet();
        children.describePet();
        children.greetPet();
        fam1.addChildren(children);
        fam1.addChildren(children2);
        mother.setSchedule(
                new Human.Schedule[]{Human.Schedule.MONDAY,Human.Schedule.FRIDAY},
                new String[]{"do shops","make dinner"});
        fam1.deleteChildren(nonChildren);
        fam1.deleteChildren(children);
        System.out.println(fam1.toString());
    }
}
