package Family;
import Pet.Pet;
import Human.Human;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Pet pet;
    private Human[] childrens = {};

    static {
        System.out.println("создается новый объект Family");
    }
    {
        System.out.println("создается новый объект класс ");
    }
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setSurname(this.father.getSurname());
    }

    public void addChildren(Human children) {
        children.setSurname(this.father.getSurname());
        Human[] newChildrens = new Human[this.childrens.length+1];
        for (int i = 0; i < newChildrens.length; i++) {
            if(i==newChildrens.length-1){
                newChildrens[i] = children;
            }
            else {
                newChildrens[i] = this.childrens[i];
            }
        }
        this.childrens = newChildrens;
    }
    public void deleteChildren(Human children) {
        Human[] newChildrens = new Human[this.childrens.length-1];
        int i = 0;
        boolean flag = false;
        for (Human child : this.childrens) {
            if(i<newChildrens.length) {
                if (!child.equals(children)) {
                    newChildrens[i] = children;
                    i++;
                } else flag = true;
            }
        }
        String str = flag ? "Ребенка больше нет" : "Такого ребенка нет в семье";
        this.childrens = flag ? newChildrens : this.childrens;
        System.out.println(Arrays.toString(this.childrens) +str);
    }

    public String toString() {
        String childrens = "childrens - ";
        System.out.println(this.childrens.length);
        for (Human children : this.childrens) {
            childrens += children.toString() +",";
        }
        return String.format(
                "Mother - %s Father -%s,"+childrens,
                this.mother.toString(),this.father.toString()
        );
    }
    public int countFamily() {
        return 2 + childrens.length;
    }
    public void finalize() {
        System.out.println("Удаляется объект Family" + this.toString());
    }

}