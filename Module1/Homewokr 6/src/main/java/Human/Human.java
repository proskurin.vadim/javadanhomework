package Human;
import Pet.Pet;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year = 1970;
    private int iq = new Random().nextInt( 100);
    private Pet pet = new Pet();
    public enum Schedule
    {
        MONDAY("None"),
        THUSDAY,
        THIRSDAY,
        WHENESDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY;
        String plans;
        Schedule(String plans) {
        this.plans = plans;
        }
        Schedule() {
            this.plans = "None";
        }

        public void setPlans(Schedule day,String plans) {
           day.plans = plans;
        }
    };

    private Schedule[] schedule;
    static {
        System.out.println("создается новый объект Human");
    }
    {
        System.out.println("создается новый объект класс ");
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getIq() {
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public Schedule[] getSchedule() {
        schedule = Schedule.values();
        return   schedule;
    }
    public void setSchedule(Schedule[] schedule,String[] plans) {
        for (Schedule day : Schedule.values()) {
            for(int i = 0; i < schedule.length; i++) {
                if (day.equals(schedule[i])) day.setPlans(day, plans[i]);
            }
        }
    }

    public Human() {
        this.setName("новорожденный");
        this.setSurname("ребенок");
        this.setYear(0);
        this.setIq(0);
    }

    public Human(String name,String surname) {
        this.setName(name);
        this.setSurname(surname);
    }

    public Human(String name,String surname, int year, int iq,Pet pet) {
        this.setName(name);
        this.setSurname(surname);
        this.setYear(year);
        this.setIq(iq);
        this.pet = pet;
    }

    public void greetPet () {
        System.out.println("Привет "+ this.pet.getNickname());
    }
    public void describePet () {
        String trick = (this.pet.getTrickLevel()>=50) ? "очень хитрый" : "почти не хитрый";
        System.out.println(String.format("У меня есть %s, ему %d  лет, он %s",
                this.pet.getSpecies(),this.pet.getAge(),trick));
    }
    public String toString() {
        String schedule = "";
        for (Schedule day : Schedule.values()) {
            schedule += day + ": " + day.plans +"; ";
        }
        return String.format("Human{name='%s', surname='%s', year=%d, iq=%d,schedule=%s}",
                this.getName(),this.getSurname(),this.getYear(),this.getIq(),schedule);
    }
    public boolean feedPet() {
        boolean flag = false;
        if(this.pet.getTrickLevel() > new Random().nextInt( 100)) flag = true;
         System.out.println(flag ?"Хм... покормлю ка "+this.pet.getNickname():
                 "Думаю, "+this.pet.getNickname() + " не голоден.");
         return flag;
    }
    public int hashCode() {
        return this.getName().hashCode() + this.getIq() +this.getYear();
    }
    @Override
    public boolean equals(Object obj) {
        if (this.hashCode() == obj.hashCode()) {
            return true;
        }
        return false;
    }

    public void finalize() {
        System.out.println("Удаляется объект Human" + this.toString());
    }
}
