import java.util.Random;
import java.util.Scanner;
public class Main {
    private static int win = 3;
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        String [][] field = fillField();
        System.out.println("All set. Get ready to rumble!");
        showField(field);
        while (win !=0 ) {
            showField(shoot(field));
        }
        System.out.println("You have won!");
    }

    private static String[][] fillField(){
        int i = new Random().nextInt( 4)+1;
        int j = new Random().nextInt( 4)+1;
        String [][] field = {
                {"0","1","2","3","4","5"},
                {"1","-","-","-","-","-"},
                {"2","-","-","-","-","-"},
                {"3","-","-","-","-","-"},
                {"4","-","-","-","-","-"},
                {"5","-","-","-","-","-"},
        };
        field[i][j] = "x";
        if(field[i-1][j].equals("-")) field[i-1][j] = "x";
        else field[i+2][j] = "x";
        if(field[i+1][j].equals("-")) field[i+1][j] = "x";
        else field[i-2][j] = "x";
        return field;
    }

    private static String[][] shoot(String[][] arr){
        System.out.println("Enter i");
        int i = checkOnInt(scan.next());
        System.out.println("Enter j");
        int j = checkOnInt(scan.next());
        if(arr[i][j].equals("x")) win --;
        arr[i][j] = "*";
        return arr;
    };

    private static int checkOnInt (String arg) {
        while (!arg.matches("[1-5]+")) {
                System.out.println("Arg must be number <5 and >0");
                arg = scan.next();
        }
        return Integer.valueOf(arg);
    }
    private static void showField(String[][] arr) {
        for (String[] i : arr) {
            for (String j : i) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}