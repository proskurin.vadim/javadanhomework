package Pet;
import java.util.Random;
import java.util.Arrays;
public class Pet {
    private String species;
    private String nickname;
    private int age = 0;
    private int trickLevel = new Random().nextInt( 100);;
    private String[] habits = {"нет"};

    static {
        System.out.println("создается новый объект Pet");
    }
    {
        System.out.println("создается новый объект класс " );
    }
    public String getSpecies() {
        return species;
    }
    public void setSpecies(String species) {
        this.species = species;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits ) {
        this.habits = habits;
    }

    public Pet() {
        this.setSpecies("животное");
        this.setNickname("Еще нет имени");
    }
    public Pet(String species, String nickname) {
        this.setSpecies(species);
        this.setNickname(nickname);
    }
    public Pet(String species, String nickname, int age, int trickLevel,String[] habits) {
        this.setSpecies(species);
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
    }

    public void eat() {
        System.out.println("я кушаю");
    }
    public void respond() {
        System.out.println(String.format("Привет, хозяин. Я - %s. Я соскучился!",this.getNickname()));
    }
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
    public String toString() {
    return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
            this.getSpecies(),this.getNickname(),this.getAge(),
            this.getTrickLevel(),Arrays.toString(this.getHabits())
    );
    }
}
