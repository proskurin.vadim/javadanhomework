package Family;
import Pet.Pet;
import Human.Human;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Pet pet;
    private Human[] childrens = {};

    static {
        System.out.println("создается новый объект Family");
    }
    {
        System.out.println("создается новый объект класс ");
    }
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setSurname(this.father.getSurname());
    }

    public void addChildren(Human children) {
        children.setSurname(this.father.getSurname());
        Human[] newCildrens = new Human[this.childrens.length+1];
        for (int i = 0; i < newCildrens.length; i++) {
            if(i==newCildrens.length-1){
                newCildrens[i] = children;
            }
            else {
                newCildrens[i] = this.childrens[i];
            }
        }
        this.childrens = newCildrens;
    }
    public void deleteChildren(Human children) {
        Human[] newCildrens = new Human[this.childrens.length-1];
        int i = 0;
        boolean flag = false;
        for (Human child : this.childrens) {
            if(i<newCildrens.length) {
                if (!child.equals(children)) {
                    newCildrens[i] = children;
                    i++;
                } else flag = true;
            }
        }
        String str = flag ? "Ребенка больше нет" : "Такого ребенка нет в семье";
        this.childrens = flag ? newCildrens : this.childrens;
        System.out.println(Arrays.toString(this.childrens) +str);
    }

    public int countFamily() {
        return 2 + childrens.length;
    }
    public String toString() {
        String childrens = "childrens - ";
        System.out.println(this.childrens.length);
        for (Human children : this.childrens) {
            childrens += children.toString() +",";
        }
        return String.format(
                "Mother - %s Father -%s,"+childrens,
                this.mother.toString(),this.father.toString()
        );
    }

}