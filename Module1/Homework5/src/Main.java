import Pet.Pet;
import Human.Human;
import Family.Family;
public class Main {
    public static void main(String[] args) {
        Human father = new Human("Леон", "Рене");
        Human mother = new Human("Натали", "Портман");
        Pet pet3 = new Pet("собака","Леди",5,50,new String[]{"Любит старков","Спит после 8"});
        System.out.println(pet3.toString());
        Human children = new Human("имя", "фамилия",1980 , 70, pet3,
                new String[][]{{"Понедельник","ничего"}});
        Human children2 = new Human("Vlad","Renyu");
        Human nonChildren = new Human("Vadim","None");
        Family fam1 = new Family(mother,father);
        pet3.foul();
        pet3.respond();
        pet3.foul();
        children.feedPet();
        children.describePet();
        children.greetPet();
        fam1.addChildren(children);
        fam1.addChildren(children2);
        fam1.deleteChildren(nonChildren);
        fam1.deleteChildren(children);
        System.out.println(fam1.toString());
    }
}
