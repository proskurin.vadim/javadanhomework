package player;
public class Player {
    private String name;
    private String answers ="";

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAnswers() {
        return answers;
    }
    public void setAnswers(Integer answers) {
        this.answers += answers + " ";
    }
}
