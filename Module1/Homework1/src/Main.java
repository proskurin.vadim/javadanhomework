import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

import StringToArraySort.StringToArraySort;
import player.Player;
public class Main {

    public static void main(String[] args) {
        Player player = new Player();              //подключаем Игрока
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter your name");
        player.setName(scan.next());              // сетим имя в класс Игрок
        System.out.println(player.getName() + ", Let the game begin!");
        String[] gameNumbers = randEvent();        // записываем в gameNumber случайное событие из функции randEvent
        // пока не назавем правильную дату не входим из цикла
        while (true) {
            System.out.print("When did "+gameNumbers[1]+" Did?");
            String line = scan.next();
            if (line.matches("\\d+")) {          // проверка на наличие не цифр
                int playerNumber = Integer.valueOf(line);
                player.setAnswers(playerNumber);      // сетим ответ в класс Игрок
                if (playerNumber > Integer.parseInt(gameNumbers[0])) {           //сравниваем ответ с годом
                    System.out.println("Your number is too big. Please, try again");
                } else if (playerNumber < Integer.parseInt(gameNumbers[0])) {
                    System.out.println("Your number is too small. Please, try again.");
                } else break; // если ответ правильный выходим
            }else System.out.println("You must enter only numbers");
        }
        StringToArraySort arraySort = new StringToArraySort(player.getAnswers());
        System.out.println(player.getName() + ", You won. Your tries " + Arrays.toString(arraySort.getSortedArray()));
    }
    // генерируем случайное событие
    private static String[] randEvent() {
        String[][] historyEvents = {
                {"1918","1943","1941"},
                {"WW1","WW2","German enter Polish"}
        };
        int n = new Random().nextInt( 3);
        return new String[] {historyEvents[0][n],historyEvents[1][n]};
    }
}
