import java.util.Scanner;
public class Main {

    static String notes[][] = {
            {"sunday", "do something1"},
            {"monday", "do something2"},
            {"tuesday", "do something3"},
            {"wednesday", "do something4"},
            {"thursday", "do something5"},
            {"friday", "do something6"},
            {"saturday", "do something7"}
    };
    private static Scanner scaner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("Please, input the day of the week(if you want to leave enter exit):");
            String answer = scaner.next().toLowerCase();
            if (answer.contains("change")) {
                String[] day = answer.split(" ");
                if (day.length != 2) {
                    System.out.println("Wrong command");
                } else  {
                    checkDay(day[1]);
                    changeNote(day[1]);
                }
            } else if (answer.equals("exit")) {
                System.out.println("You leave program");
                break;
            } else checkDay(answer);
        }
    }

    private static void checkDay(String arg) {
        boolean flag = false;
        for (int i = 0; i <= notes[0].length - 1; i++) {
            if (notes[i][0].equals(arg)) {
                System.out.println("Your tasks for " + notes[i][0] + " : " + notes[i][1]);
                flag = true;
            }
        }
        if(!flag) {
            System.out.println("Threre is no day or command with name " +arg);
        }
    }

    private static void changeNote(String arg) {
        for (int i = 0; i <= notes[0].length - 1; i++) {
            if (notes[i][0].equals(arg)) {
                System.out.println("Enter new plans for " + arg);
                String answer = scaner.next();
                notes[i][1] = answer;
                System.out.println("Your new plans for " + arg + " : " + answer);
            }
        }
    }
}
