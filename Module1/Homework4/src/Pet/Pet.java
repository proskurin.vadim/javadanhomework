package Pet;
import java.util.Random;
import java.util.Arrays;
public class Pet {
    public String species;
    public String nickname;
    public int age = 0;
    public int trickLevel = new Random().nextInt( 100);;
    private String[] habits = {"нет"};

    public Pet() {
        this.species = "животное";
        this.nickname = "Еще нет имени";
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel,String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    public void eat() {
        System.out.println("я кушаю");
    }
    public void respond() {
        System.out.println(String.format("Привет, хозяин. Я - %s. Я соскучился!",this.nickname));
    }
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
    @Override
    public String toString() {
    return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
            this.species,this.nickname,this.age,this.trickLevel,Arrays.toString((this.habits)));
    }
}
