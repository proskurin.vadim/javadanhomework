package Human;
import Pet.Pet;

import java.util.Random;

public class Human {
    public String name;
    public String surname;
    private int year = 1970;
    private int iq = new Random().nextInt( 100);
    private Pet pet = new Pet();
    public String [][] schedule = {{"Нет","Планов"}};
    private Human mother;
    private Human father;

    public Human() {
        this.name = "новорожденный";
        this.surname = "ребенок";
        this.year = 0;
        this.iq = 0;
        this.mother = new Human("мама","",null,null);
        this.father = new Human("папа","",null,null);
    }

    public Human(String name,String surname) {
        this.name = name;
        this.surname = surname;
        this.mother = new Human("мама",surname,null,null);
        this.father = new Human("папа",surname,null,null);
    }
    public Human(String name,String surname,Human mother,Human father) {
        this.name = name;
        this.surname = surname;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name,String surname, int year, int iq, Human mother, Human father, Pet pet, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.mother = mother;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
        this.father = father;
    }

    public void greetPet () {
        System.out.println("Привет "+ this.pet.nickname);
    }
    public void describePet () {
        String trick = (this.pet.trickLevel>=50) ? "очень хитрый" : "почти не хитрый";
        System.out.println(String.format("У меня есть %s, ему %d  лет, он %s",
                this.pet.species,this.pet.age,trick));
    }
    @Override
    public String toString() {
        return String.format("Human{name='%s', surname='%s', year=%d, iq=%d, mother='%s', father'%s', pet=%s",
                this.name,this.surname,this.year,this.iq,this.mother.name + " "+ this.mother.surname,
                this.father.name + " " + this.father.surname,this.pet.toString());
    };
    public boolean feedPet() {
        boolean flag = false;
        if(this.pet.trickLevel > new Random().nextInt( 100)) flag = true;
         System.out.println(flag ?"Хм... покормлю ка "+this.pet.nickname:
                 "Думаю, "+this.pet.nickname + " не голоден.");
         return flag;
    };
 }
