import Pet.Pet;
import Human.Human;
public class Main {
    public static void main(String[] args) {
        Human mother = new Human("Леон", "Рене");
        Human father = new Human("Натали", "Портман");
        Human human1 = new Human();
        Human human3 = new Human("имя", "фамилия", mother, father);
        Pet pet1 = new Pet();
        Pet pet2 = new Pet("собака","Призрак");
        Pet pet3 = new Pet("собака","Леди",5,50,new String[]{"Любит старков","Спит после 8"});
        System.out.println(human1.toString());
        System.out.println(mother.toString());
        System.out.println(human3.toString());
        System.out.println(pet1.toString());
        System.out.println(pet2.toString());
        System.out.println(pet3.toString());
        Human human4 = new Human("имя", "фамилия",1980 , 70, mother, father, pet3,
                new String[][]{{"Понедельник","ничего"}});
        human4.describePet();
        human4.greetPet();
        pet3.foul();
        pet3.respond();
        pet3.foul();
        human4.toString();
        human4.feedPet();
    }
}
