package Human.Woman;

import Human.Human;
import Pet.Pet;

public final class Woman  extends Human {
    public Woman() {
        super();
    }

    public Woman(String name,String surname) {
        super(name,surname);
    }
    public Woman(String name,String surname, int year, int iq) {
        super(name,surname,year,iq);
    }
    public Woman(String name, String surname, int year, int iq, Pet pet) {
        super(name,surname,year,iq,pet);
    }

    @Override
    public void greetPet() {
        System.out.println("Привет " + this.pet.getNickname());
    }
    public void makeup() {
        System.out.println(" Красимся ");
    }
}
