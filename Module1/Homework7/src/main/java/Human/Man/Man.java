package Human.Man;

import Human.Human;
import Pet.Pet;

public final class Man extends Human {
    public Man() {
        super();
    }

    public Man(String name,String surname) {
        super(name,surname);
    }

    public Man(String name,String surname, int year, int iq) {
        super(name,surname,year,iq);
    }

    public Man(String name, String surname, int year, int iq, Pet pet) {
        super(name,surname,year,iq,pet);
    }

    @Override
    public void greetPet() {
        System.out.println("Ко мне "+ this.pet.getNickname());
    }

    public void repairCar() {
        System.out.println(" Чиним машину ");
    }
}
