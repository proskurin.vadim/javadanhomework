package Pet.Fish;

import Pet.Pet;

public class Fish extends Pet {
    Species species = Species.FISH;
    public Fish(String nickname) {
        super(nickname);
    }
    public Fish( String nickname, int age, int trickLevel,String[] habits) {
        super(nickname,age,trickLevel,habits);
    }

    @Override
    public void respond() {
        System.out.println("Буль-буль я "+this.getNickname());
    }
}