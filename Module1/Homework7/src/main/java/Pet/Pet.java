package Pet;

import java.util.Arrays;
import java.util.Random;
public  abstract class Pet {
    public  enum  Species  {
        UNKNOWN (false,0,false),
        ROBOCAT(false,4,false),
        CAT(false,4,false),
        FISH(false,0,false),
        DOG(false,4,false);
        boolean canFlay;
        int numOfLegs;
        boolean hasFur;
        Species(boolean canFlay,int numOfLegs,boolean hasFur) {
            this.canFlay = canFlay;
            this.numOfLegs = numOfLegs;
            this.hasFur = hasFur;
        }
        Species(){
            this.canFlay = false;
            this.numOfLegs = 4;
            this.hasFur = true;
        }
    };
    public Species  specie = Species.UNKNOWN;
    private String nickname;
    private int age = 0;
    private int trickLevel = new Random().nextInt( 100);;
    private String[] habits = {"нет"};

    static {
        System.out.println("создается новый объект Pet");
    }
    {
        System.out.println("создается новый объект класс " );
    }
    public Species getSpecies() {
        return specie;
    }
    public void setSpecies(Species specie) {
        this.specie = specie;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits ) {
        this.habits = habits;
    }

    public Pet() {
        this.setNickname("Еще нет имени");
    }
    public Pet(String nickname) {
        this.setNickname(nickname);
    }
    public Pet(String nickname, int age, int trickLevel,String[] habits) {
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
    }

    public void eat() {
        System.out.println("я кушаю");
    }
    public  abstract void respond() ;
    public interface foul {
        public  void foul();
    }
    public String toString()    {
    return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s , canFlay=%s,numOfLegs=%s,hasFur=%s}",
            this.getSpecies(),this.getNickname(),this.getAge(),
            this.getTrickLevel(),Arrays.toString(this.getHabits()),
            this.getSpecies().canFlay,this.getSpecies().numOfLegs,this.getSpecies().hasFur
    );
    }
    public void finalize() {
        System.out.println("Удаляется объект Pet" + this.toString());
    }
}
