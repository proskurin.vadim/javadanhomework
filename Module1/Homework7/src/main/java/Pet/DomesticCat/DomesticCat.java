package Pet.DomesticCat;

import Pet.Pet;
import Pet.BadPet;

import java.util.Random;

public class DomesticCat extends Pet implements BadPet {
    Species species = Species.CAT;
    public DomesticCat(String nickname) {
        super(nickname);
        setSpecies(Species.DOG);
    }
    public DomesticCat( String nickname, int age, int trickLevel,String[] habits) {
        super(nickname,age,trickLevel,habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Аааа, опять ты  я "+this.getNickname());
    }
    public void foul() {
        String foul = fouls[(new Random().nextInt(fouls.length-1))];
        System.out.println("Я испортил " + foul);
    }
}