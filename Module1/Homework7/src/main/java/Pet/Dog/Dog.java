package Pet.Dog;

import Pet.Pet;
import Pet.BadPet;

import java.util.Random;

public class Dog extends Pet implements BadPet {
    public Dog(String nickname) {
        super(nickname);
        setSpecies(Species.DOG);
    }
    public Dog( String nickname, int age, int trickLevel,String[] habits) {
        super(nickname,age,trickLevel,habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Привет хозяин я "+this.getNickname());
    }
    public void foul() {
        String foul = fouls[(new Random().nextInt(fouls.length-1))];
        System.out.println("Я испортил " + foul);
    }
}