package Pet.RoboCat;

import Pet.Pet;
import Pet.BadPet;

public class RoboCat extends Pet implements BadPet {
   Species species = Species.ROBOCAT;
    public RoboCat(String nickname) {
        super(nickname);
        setSpecies(Species.DOG);
    }
    public RoboCat( String nickname, int age, int trickLevel,String[] habits) {
        super(nickname,age,trickLevel,habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Привет мешок кожи я "+this.getNickname());
    }
    public void foul() {
        System.out.println("Убить Сару Конор");
    }
}