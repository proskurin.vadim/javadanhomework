import Family.Family;
import Human.Human;
import Human.Man.Man;
import Human.Woman.Woman;
import Pet.Dog.Dog;
import Pet.Pet;

public class Main {
    public static void main(String[] args) {
        Human father = new Man("Леон", "Рене");
        Human mother = new Woman("Натали", "Портман");
        Dog pet3 = new Dog("Леди",5,50,new String[]{"Любит старков","Спит после 8"});
        System.out.println(pet3.toString());
        Family fam1 = new Family(mother,father);

        Human children = fam1.bornChild();
        System.out.println(children.toString());
        pet3.respond();
        pet3.foul();
        System.out.println(pet3.toString());
        System.out.println(fam1.toString());
    }
}
