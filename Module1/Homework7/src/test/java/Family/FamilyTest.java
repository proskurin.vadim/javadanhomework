package Family;

import Human.Human;
import Human.Man.Man;
import Human.Woman.Woman;

import static org.junit.Assert.*;
public class FamilyTest {

    private Family family;
    private Human c1;
    private int count;
    @org.junit.Before
    public void init() {
        Human m = new Man("m","h");
        Human f = new Woman("f","h");
        c1 = family.bornChild();
        Human c2 = family.bornChild();
        Human c3 = family.bornChild();
        family = new Family(m,f);
        family.addChildren(c1);
        family.addChildren(c2);
        family.addChildren(c3);
        count = family.countFamily();
    }
    @org.junit.After
    public void tearDown() {
        family = null;
    }

    @org.junit.Test
    public void testAddChildren() {
        Human c4 = family.bornChild();
        family.addChildren(c4);
        assertEquals("add child",count+1,family.countFamily());
    }

    @org.junit.Test
    public void testDeleteChildren() {
        family.deleteChildren(c1);
        assertEquals("dell child",count,count-1);
    }

    @org.junit.Test
    public void testCountFamily() {
        assertEquals("equals",count,5);
        assertNotEquals("not equals",count,55);
    }
}
