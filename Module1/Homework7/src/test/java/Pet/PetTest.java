package Pet;
import Pet.DomesticCat.DomesticCat;

import static org.junit.Assert.*;

public class PetTest {
    private Pet pet;
    @org.junit.Before
    public void init() {
        pet = new DomesticCat("pet");
    }
    @org.junit.After
    public  void tearDown() {
        pet = null;
    }
    @org.junit.Test
    public void testToString() {
        assertTrue(pet.toString(),true);
    }
}
